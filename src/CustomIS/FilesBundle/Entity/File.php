<?php

declare(strict_types = 1);

namespace CustomIS\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class File
 *
 * @ORM\Entity()
 * @ORM\Table(schema="files")
 * @ORM\InheritanceType("JOINED")
 */
class File implements \JsonSerializable
{
    /**
     * @var \League\Flysystem\File
     */
    protected $flysystemFile;

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="string", length=40)
     */
    private $contentSHA1Hash;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $mimeType;

    /**
     * @var string|int
     *
     * @ORM\Column(type="bigint")
     */
    private $fileSize;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * File constructor.
     *
     * @param string     $contentSHA1Hash
     * @param string     $mimeType
     * @param string|int $fileSize
     */
    public function __construct(string $contentSHA1Hash, string $mimeType, $fileSize)
    {
        $this->contentSHA1Hash = $contentSHA1Hash;
        $this->mimeType = $mimeType;
        $this->fileSize = $fileSize;
        $this->created = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return \League\Flysystem\File
     */
    public function getFlysystemFile(): \League\Flysystem\File
    {
        return $this->flysystemFile;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return str_replace(
            '\\',
            '/',
            substr($this->getContentSHA1Hash(), 0, 2).DIRECTORY_SEPARATOR.$this->getContentSHA1Hash()
        );
    }

    /**
     * @return string
     */
    public function getContentSHA1Hash(): string
    {
        return $this->contentSHA1Hash;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id'        => $this->getId(),
            'file_size' => $this->getFileSize(),
            'mime_type' => $this->getMimeType(),
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->getContentSHA1Hash();
    }

    /**
     * @return int|string
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }
}
