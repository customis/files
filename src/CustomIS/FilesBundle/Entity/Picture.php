<?php

declare(strict_types = 1);

namespace CustomIS\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Picture
 *
 * @ORM\Entity()
 * @ORM\Table(schema="files")
 */
class Picture extends File
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $width;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $height;

    /**
     * Picture constructor.
     *
     * @param string $contentSHA1Hash
     * @param string $mimeType
     * @param int    $fileSize
     * @param int    $width
     * @param int    $height
     */
    public function __construct(string $contentSHA1Hash, string $mimeType, int $fileSize, int $width, int $height)
    {
        parent::__construct($contentSHA1Hash, $mimeType, $fileSize);
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }
}
