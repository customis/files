<?php

declare(strict_types = 1);

namespace CustomIS\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class FileInfo
 *
 * @ORM\Entity()
 * @ORM\Table(schema="files")
 */
class FileInfo implements \JsonSerializable
{
    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="CustomIS\FilesBundle\Entity\File")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false, referencedColumnName="content_sha1hash")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4096)
     */
    private $fileName;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=4096, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * FileInfo constructor.
     *
     * @param File   $file
     * @param string $fileName
     */
    public function __construct(File $file, string $fileName)
    {
        $this->file = $file;
        $this->fileName = $fileName;
        $this->created = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id'        => $this->getId(),
            'title'     => $this->getTitle(),
            'file'      => $this->getFile(),
            'file_name' => $this->getFileName(),
        ];
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     */
    public function setTitle(?string $title)
    {
        $this->title = $title;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }
}
