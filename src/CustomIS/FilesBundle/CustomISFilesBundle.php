<?php

declare (strict_types = 1);

namespace CustomIS\FilesBundle;

use CustomIS\FilesBundle\DependencyInjection\Compiler\FlysystemHashPluginPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CustomISDoctrineBundle
 */
class CustomISFilesBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new FlysystemHashPluginPass());
    }
}
