<?php

declare(strict_types = 1);

namespace CustomIS\FilesBundle\DependencyInjection;

use CustomIS\DoctrineBundle\Doctrine\Platform\PostgreSQLPlatform;
use CustomIS\DoctrineBundle\Doctrine\Types\CZKCurrencyType;
use CustomIS\DoctrineBundle\Doctrine\Types\DecimalType;
use CustomIS\DoctrineBundle\Doctrine\Types\IntervalType;
use CustomIS\DoctrineBundle\Doctrine\Types\IntRangeType;
use CustomIS\DoctrineBundle\Doctrine\Types\PointType;
use CustomIS\DoctrineBundle\Doctrine\Types\PolygonType;
use CustomIS\DoctrineBundle\Doctrine\Types\RepeatType;
use CustomIS\FilesBundle\Service\FileSaver;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class CustomISFilesExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $definition = $container->getDefinition(FileSaver::class);
        $definition->setArgument('$filesystem', new Reference($config['filesystem_name']));

    }
}
