<?php

namespace CustomIS\FilesBundle\DependencyInjection\Compiler;

use Emgag\Flysystem\Hash\HashPlugin;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class FlysystemHashPluginPass
 */
class FlysystemHashPluginPass implements CompilerPassInterface
{
    public const ONEUP_FLYSYSTEM_ID = 'oneup_flysystem.filesystem';

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container): void
    {
        if ($container->hasDefinition(self::ONEUP_FLYSYSTEM_ID)) {
            $serviceDefinition = $container->getDefinition(self::ONEUP_FLYSYSTEM_ID);
            $serviceDefinition->addMethodCall('addPlugin', [new Reference(HashPlugin::class)]);
        }
    }
}
