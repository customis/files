<?php

declare(strict_types = 1);

namespace CustomIS\FilesBundle\Exception;

/**
 * Class FileNotSavedException
 */
class FileNotSavedException extends \RuntimeException
{
}
