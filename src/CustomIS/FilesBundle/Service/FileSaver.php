<?php

declare(strict_types = 1);

namespace CustomIS\FilesBundle\Service;

use CustomIS\FilesBundle\Entity\File;
use CustomIS\FilesBundle\Entity\FileInfo;
use CustomIS\FilesBundle\Entity\Picture;
use CustomIS\FilesBundle\Exception\FileNotSavedException;
use Doctrine\ORM\EntityManager;
use League\Flysystem\Filesystem;
use Oneup\UploaderBundle\Uploader\File\FlysystemFile;

/**
 * Class FileSaver
 */
class FileSaver
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * FileSaver constructor.
     *
     * @param EntityManager $entityManager
     * @param Filesystem    $filesystem
     */
    public function __construct(EntityManager $entityManager, Filesystem $filesystem)
    {
        $this->entityManager = $entityManager;
        $this->filesystem = $filesystem;
    }

    /**
     * @param string        $data
     * @param string        $filename
     * @param callable|null $persistFunc
     *
     * @return FileInfo
     */
    public function save(string $data, string $filename, ?callable $persistFunc = null): FileInfo
    {
        $sha1Hash = sha1($data);
        $path = substr($sha1Hash, 0, 2).DIRECTORY_SEPARATOR.$sha1Hash;
        $this->filesystem->put($path, $data);
        $dataObject = new FlysystemFile($this->filesystem->get($path), $this->filesystem);

        $persistFunc = $persistFunc ?? function (FlysystemFile $dataObject) {
                $mimeType = $dataObject->getMimetype();
                if (strpos($mimeType, 'image') === 0) {
                    /** @var int $width */
                    /** @var int $height */
                    [$width, $height] = getimagesizefromstring($dataObject->read());

                    return new Picture(
                        $dataObject->hash('sha1'),
                        $dataObject->getMimetype(),
                        $dataObject->getSize(),
                        $width,
                        $height
                    );
                }

                return new File(
                    $dataObject->hash('sha1'),
                    $dataObject->getMimetype(),
                    $dataObject->getSize()
                );
            };

        /** @var File|null $fileEntity */
        try {
            return $this->entityManager->transactional(function (EntityManager $entityManager) use (
                $persistFunc,
                $filename,
                $dataObject,
                $sha1Hash
            ) {
                if (null === ($fileEntity = $entityManager->find(File::class, $sha1Hash))) {
                    $fileEntity = $persistFunc($dataObject);
                    $entityManager->persist($fileEntity);
                }

                $fileInfo = new FileInfo(
                    $fileEntity,
                    $filename
                );
                $entityManager->persist($fileInfo);

                return $fileInfo;
            });
        } catch (\Throwable $e) {
            $this->throwException($e);
        }
    }

    /**
     * @param \Exception $e
     *
     * @throws FileNotSavedException
     */
    private function throwException(\Exception $e): void
    {
        throw new FileNotSavedException('', 0, $e);
    }
}
