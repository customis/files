<?php

declare(strict_types = 1);

namespace CustomIS\FilesBundle\EventListener;

use CustomIS\FilesBundle\Entity\File;
use League\Flysystem\Filesystem;

class FileInjectFilesystemListener
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * FileInjectFilesystemListener constructor.
     *
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param File $file
     */
    public function update(File $file): void
    {
        $reflection = new \ReflectionObject($file);
        $property = $reflection->getProperty('flysystemFile');
        $property->setAccessible(true);
        $property->setValue($file, $this->filesystem->get($file->getFilePath()));
        $property->setAccessible(false);
    }
}
